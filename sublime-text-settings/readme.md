# Sublime text 2 Configuration

## Packages

* [**alignment**](https://github.com/wbond/sublime_alignment)  simple key-binding for aligning multi-line and multiple selections
* [**Color Highlighter**](https://github.com/Monnoroch/ColorHighlighter) - previews hexadecimal color values by underlaying the selected hex codes.
* [**CSS3**](https://github.com/y0ssar1an/CSS3) - The most complete CSS support for Sublime Text 3
* [**EditorConfig**](https://github.com/sindresorhus/editorconfig-sublime#readme) - Coding style for configfiles
* [**Emmet**](https://github.com/sergeche/emmet-sublime) - Official Emmet plugin (previously called Zen Coding) for Sublime Text.
* [**Gitgutter**](https://github.com/jisaacks/GitGutter) - A sublime text plugin to show an icon in the gutter area indicating whether a line has been inserted, modified or deleted.
* [**JavaScriptNext - ES6 Syntax **](https://github.com/Benvie/JavaScriptNext.tmLanguage) - JavaScript language definition for TextMate and SublimeText 3
* [**Monokai Extended**](https://github.com/jonschlinkert) - Extends Monokai from Soda with additional syntax highlighting for Markdown, LESS, HTML, Handlebars and more.
* [**Package Control**](https://sublime.wbond.net/installation) - Allow to install more packages
* [**Pretty JSON**](https://github.com/dzhibas/SublimePrettyJson) - Prettify / minify JSON files
* [**Project Manager**](https://github.com/randy3k/Project-Manager) - Switch projects without browsing in Sublime Text
* [**Seti_UI**](https://packagecontrol.io/packages/Seti_UI) - A dark colored UI theme for Sublime Text 3 with custom file icons.
* [**SideBarEnhancements**](https://github.com/titoBouzout/SideBarEnhancements) - Add more actions for sublime side bar
* [**SublimeLinter**](https://github.com/SublimeLinter/SublimeLinter3) - Interactive code linting framework for Sublime Text 3
* [**SublimeLinter-contrib-eslint**](https://github.com/roadhump/SublimeLinter-eslint) - This linter plugin for SublimeLinter provides an interface to ESLint
* [**Syntax Hightlighting for Sass**](https://github.com/P233/Syntax-highlighting-for-Sass) - Perfect syntax highlighting for both SCSS and Sass.

* [**Wordhighlight**](https://github.com/SublimeText/WordHighlight) - This plugin highlights all copies of a word that's currently selected
* [**DocBlockr**](https://github.com/Warin/Sublime/tree/master/DocBlockr) - code comment block


## Settings user

	{
		"auto_complete_triggers":
		[
			{
				"characters": "<",
				"selector": "text.html"
			},
			{
				"characters": " ",
				"selector": "text.html meta.tag"
			}
		],
		"caret_extra_bottom": 3,
		"caret_extra_width": 2,
		"close_windows_when_empty": true,
		"folder_exclude_patterns":
		[
			".svn",
			".git",
			".hg",
			"CVS",
			"node_modules",
			".tmp"
		],
		"font_size": 12.0,
		"highlight_line": true,
		"ignored_packages":
		[
			"Vintage"
		],
		"lint_mode": "load/save",
		"mark_occurrences_on_gutter": true,
		"overlay_scroll_bars": "enabled",
		"show_full_path": true,
		"tab_size": 2,
		"theme": "Seti_orig.sublime-theme",
		"translate_tabs_to_spaces": true,
		"trim_trailing_white_space_on_save": true
	}


## Keybindings user

	[
		{ "keys": ["super+shift+slash"], "command": "toggle_comment", "args": { "block": false } },
		{ "keys": ["super+shift+;"], "command": "toggle_comment", "args": { "block": true } },
		{ "keys": ["super+)"], "command": "indent" },
		{ "keys": ["super+["], "command": "unindent" }
	]

## Snippets

To install snippets go `Tools > New snippets...` then save