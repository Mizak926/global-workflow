# Setting up a seperate Github and Bitbucket account

*NOTE: THIS GUIDE WORKS AS OF JAN 9 2014.*

If you want to have private repositories at Bitbucket and public repositories at 
Github, then this is the guide for you.

This guide assumes: 

+ Git is set up for a single github user using https, not ssh 
+ There is only a *known_hosts* file in `~/.ssh/`
+ The computer is running Mac OSX 10.7 or greater
+ You already have an account at [Github](http://www.github.com) and another at [Bitbucket](http://bitbucket.org)