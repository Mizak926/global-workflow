# Setup for Iterm
This setup also works for Mac OS X Terminal and Iterm 2.

## 'subl' as command line text editor

### Pre-requisites

Sublime text 2 have to be installed on the mac.

    cd ~                            // go to home directory
    mkdir bin                       // if bin folder does not exist
    
Create alias for 'subl' command

    sudo ln -s "/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl" /usr/bin/subl
    
Edit bash_profile
    
    cd ~ 
    vim .bash_profile               // This will open or create a .bash_profile file 
    
In .bash_profile add this line
    
    export PATH=$PATH:~/bin

Then save and quit and refresh the bash_profile
    
    :wq                             // save and close file
    source ~/.bash_profile          // evaluate and refresh changes 
    
Now test the command

    $ -> subl --help


## Simple git alias and custom colors for git command

open or create ".gitconfig" file

    subl ~/.gitconfig
     
Insert content from /iterm-settings/.gitconfig to your .gitconfig then refresh it with

    source ~/.gitconfig
    

## branch name in prompt with custom colors

To get a nicer prompt with branch name and custom colors, add the content of bash_profile.sh to yout .bash_profile

    source ~/.bash_profile
    
Restart your terminal and have fun !


source : http://www.git-attitude.fr/2013/05/22/prompt-git-qui-dechire/

## A better git log

    git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

Then use git lg

source : https://coderwall.com/p/euwpig