# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

#git aliases
alias lg="git log"
alias refreshcache="rm -r --cached ."
alias ad="git add"
alias st="git status"
alias ci="git commit"
alias br="git branch"
alias co="git checkout"
alias df="git diff"
alias stash="git stash"
alias push="git push"
alias pull="git pull"
alias remote="git remote"
alias dc="git diff--cached"
alias clone="git clone"

#utilites
alias pyserve="python -m SimpleHTTPServer"

#babel
alias bn="babel-node --presets es2015"

#apps
alias mou="open /Applications/Mou.app"