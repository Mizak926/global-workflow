# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

#git aliases
alias lg="git log --graph --decorate --oneline"
alias refreshcache="git rm -r --cached ."
alias ad="git add"
alias st="git status -s"
alias ci="git commit"
alias br="git branch"
alias co="git checkout"
alias df="git diff"
alias stash="git stash"
alias push="git push"
alias pull="git pull"
alias remote="git remote"
alias dc="git diff--cached"
alias clone="git clone"
alias lgall="git log --all --graph --decorate --oneline --simplify-by-decoration"
alias merge="git merge"
#utilites
alias pyserve="python -m SimpleHTTPServer"

#babel
alias bn="babel-node --presets es2015"

#mediaserver
alias osmc="sudo ssh osmc@192.168.1.130 -p ntgmlp"