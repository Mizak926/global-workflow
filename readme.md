# Global workflow

Here I regroup all of my development workflow. I will list here all my settings and tools for development.

## Playing with
* [**Sublime text 3**](http://www.sublimetext.com/3) - the simpliest editor for now.
* [**Iterm2**](http://www.iterm2.com/#/section/home) - A better Terminal


## Working faster with
* [**Alfred 2**](http://www.alfredapp.com/) - A application launcher and more...
* [**Better Touch Tool**](http://www.boastr.net/) - Windows snapping screen behaviour for mac OS X


## Git control & deployment
* [**CyberDuck**](http://cyberduck.io/) - Elegant FTP Client
* [**Gitup**](http://gitup.co/) - The Git interface you've been missing all your life has finally arrived.

## Team project Management
* [**Trello**](https://trello.com/) - Trello is the free, flexible, and visual way to organize anything with anyone.
* [**Todoist**](https://todoist.com) - A simple and elegant task manager
* [**Basecamp**](https://basecamp.com/) - A nice and elegant project management tool. Easy-to-use, simple task and notes, it goes straightforward to what you need or what du do. 60-day unlimited-use free trial then $20/month.

## Quick look plugins tools

Base on the following this [**list**](https://github.com/sindresorhus/quick-look-plugins), I use:

- QuickLookJSON
- QLColorCode
- QLMarkdown

